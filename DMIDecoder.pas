program DMIDecoder;

{$mode objfpc}{$H+}{$J-}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, DMIParser
  { you can add units after this };
const
  DMISample = '# dmidecode 3.1' + LineEnding +
'Getting SMBIOS data from sysfs.' + LineEnding +
'SMBIOS 2.6 present.' + LineEnding +
' ' + LineEnding +
'Handle 0x0001, DMI type 1, 27 bytes' + LineEnding +
'System Information' + LineEnding +
'        Manufacturer: LENOVO' + LineEnding +
'        Product Name: 20042' + LineEnding +
'        Version: Lenovo G560' + LineEnding +
'        Serial Number: 2677240001087' + LineEnding +
'        UUID: CB3E6A50-A77B-E011-88E9-B870F4165734' + LineEnding +
'        Wake-up Type: Power Switch' + LineEnding +
'        SKU Number: Calpella_CRB' + LineEnding +
'        Family: Intel_Mobile' + LineEnding;
var
  DMITable: TDMITable;
begin

  DMITable := TDMITable.Create(DMISample);

  DMITable.Parse;

  ReadLn;


end.

