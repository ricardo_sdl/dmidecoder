unit DMIParser;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

  { TDMIProperty }

  TDMIProperty = class
    Key: String;
    Value: String;
    Values: TStringList;
  end;

  { TDMISection }

  TDMISection = class
    Handle: String;
    Title: String;
    private
      Properties: array of TDMIProperty;
  end;

  { TDMITable }

  TDMITable = class
    public
      constructor Create(DMISource: String);
      function Parse: Boolean;
      function IsParsed: Boolean;
    private
      Parsed: Boolean;
      Source: String;
      Sections: array of TDMISection;




  end;

  function ParseDMI(DMISource: String): TDMITable;

implementation

type
  TPaserState = (NoOp, SectionName, ReadKeyValue, ReadList);

function ParseDMI(DMISource: String): TDMITable;
begin
  Result := TDMITable.Create(DMISource);
end;

{ TDMITable }

constructor TDMITable.Create(DMISource: String);
begin
  inherited Create;
  Self.Source := DMISource;
  Self.Parsed := False;
end;

function TDMITable.Parse: Boolean;
var
  Lines: TStringList;
begin

  Lines := TStringList.Create;
  Lines.LineBreak := LineEnding;
  Lines.Text := Self.Source;

  WriteLn(Lines.Strings[1]);


  Result := False;


end;

function TDMITable.IsParsed: Boolean;
begin
  Result := Self.Parsed;
end;

end.

